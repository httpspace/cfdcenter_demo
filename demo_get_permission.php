<?php 
include 'cfdcenterClass.php';

/*
|-------------------------------------------------------
| 取得權限 // 如果您需要取得權限資料並實作can才看這隻檔案
|-------------------------------------------------------
*/

$input = [
	'id' => 'obov',
	'password' => 'obov'
];

//取得使用者登入資訊	
Cfdcenter::login($input);

//會員資料
 
//取得使用者登入者權限資料(必須登先登入才能查看
echo '[登入者]這是目前登入者權限資料<br /><pre>';
var_dump( Cfdcenter::permissions()->all());
echo '[登入者]這是過濾*.edit_權限資料<br /><pre>';
var_dump(Cfdcenter::permissions()->filter('*', 'edit_'));



//所有權限資料
echo '[系統所有權限]這是所有權限資料<br /><pre>';
var_dump(Cfdcenter::permissions(true)->all());
echo '[系統所有權限]這是過濾權 nav.edit_ 限資料<br /><pre>';
var_dump(Cfdcenter::permissions(true)->filter('nav', 'edit_'));
echo '[系統所有權限]這是過濾nav.*權限資料<br /><pre>';
var_dump(Cfdcenter::permissions(true)->filter('nav', '*'));
