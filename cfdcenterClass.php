<?php 
	class Cfdcenter{
		
		public static $token = '79ee0e4c13129b3dcade38deddd769fb';
		//版本號
		private static $version = '0.79';
		//設定環境 dev/online
		private static $cache_path = '/home/user123/public_html/cache/';
		//設定更新時間
		private static $cookie_time = 1200;
		//是否為開發中
		private static $dev = false; 
		//last_update_at
		public static $updated_at = '';
		//config
		public static $config = null;
		//self
		public static $self = null;
		//暫存區
		public static $deal = [];
		
		public static $has_check = false;
		
		public static function set_config($config){
			
			self::$cache_path = isset($config['cache_path']) ? $config['cache_path'] : '';
			self::$token      = isset($config['token']) ? $config['token'] : '';
			self::$dev        = isset($config['dev']) ? $config['dev'] : false;
		}
		
		public static function set_cache_path($path){
			
			self::$cache_path = $path;
		}
		
		public static function httpPost($input, $url, $json = false){

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
			$response = curl_exec($ch);

			return ($json) ? json_decode($response, 1) : $response;
		}
		
		public static function api($method = null, $input = [], $json = true){
			static::$token = isset($input['access_token']) ? $input['access_token'] : static::$token;
			
			if(!static::$token) return false;
			
			$input['access_token'] = static::$token;
			
			$api_path = (self::$dev) ? 'permission_dev.php' : 'permission.php';
			
			$path = 'http://center.cfd888.info/api/'.$api_path.$method;
			
			return self::httpPost($input, $path, $json);	
		}
		
		public static function curl_get_config($json=true){
			$token = static::$token;
			$command = "curl -X GET 'http://center.cfd888.info/api/permission.php?method=install_config&access_token={$token}'";
			$output = shell_exec($command);
			$data = @json_decode($output, 1);
			if($data['status']){
				return ($json) ? json_decode($data['data'], 1) : $data['data'];
			}
			return false;
		}
		//版本檢查
		public static function check_version(){
			if(static::$has_check) return true;
			$data_client = self::read_file();
			
			if(!$data_client){
				
				self::install();
				return true;
			}
			
			$data_server = self::api('?method=install_config');
			$data_server = ($data_server['status']) ? json_decode($data_server['data'], 1) : false;
			
			static::$has_check = true;
			
			if(!$data_server){
				throw new Exception('server connect error!!');
				return false;
			}
			
			if($data_client['date'] !== $data_server['date']){
				
				self::install();
				return true;
				
			}
			return true;
		}
		
		public static function install(){
			$response = self::api('?method=install_config');
			// $response = self::curl_get_config(false);
			// showx($response);
			if($response['status']){
				try{
					$file = self::$cache_path.self::$token.'.json';
					chmod(self::$cache_path, 0777); 
					file_put_contents($file, $response['data']);
					$data = self::read_file();
					return true;
				} catch (Exception $e) {
					
					// var_dump($e);				
				}
			} 
			
			return false;
		}
		
		//取得實體設定檔
		public static function read_file(){
			try{
				$file = self::$cache_path.self::$token.'.json';
				if(!file_exists($file)) return false;
				$data = json_decode(file_get_contents($file), 1);
				self::$updated_at = $data['date'];
				self::$config     = $data;
				return $data;
			} catch (Exception $e) {
				return false;
			} 
		}
		
		//取得設定檔
		public static function get_config(){

			if(!isset($_COOKIE[static::$token.'_updated_at'])){
				static::check_version();	
			} 
			
			if(self::$config == null){
				self::read_file();
			}
			
			setcookie(static::$token.'_updated_at', date('Y-m-d H:i:s'), time()+self::$cookie_time, '/'); //更新時間
			return self::$config;
		}
		
		//new self
		public static function init(){
			
			return (static::$self == null) ? new self() : static::$self;
		}
		
		//顯示暫存區全部權限
		public function all(){
			
			return array_values(self::$deal);
		}
		
		//權限AS key
		public function _all(){
			
			return array_flip(self::$deal);
		}
		
		//顯示暫存區過濾權限
		public function filter($object = '*', $action = '*'){
			
			$data = self::$deal;
			
			if($object == '*' && $action == '*') return $data;
			
			$object = ($object == '*') ? '.+' : $object;
			$action = ($action == '*') ? '.+' : $action;

			$return = [];
			
			$regular = "/{$object}\.{$action}/";

			foreach($data as $val){

				if(self::is_match($regular, $val)){
					$return[] = $val;
				}
			}
			return $return;
		}
		//是否吻合正規式
		private static function is_match($regular, $val){
			
			preg_match($regular, $val, $output_array);
			return ($output_array);
		}
		
		//取得權限
		public static function permissions($is_all = false){

			return ($is_all) ? self::permissions_server() : self::permissions_login();
		}
		
		//取得server權限
		public static function permissions_server(){
			self::get_config();
			self::$deal = self::$config['permission'];
			
			return self::init();
		}
		
		//目前登入者權限
		public static function permissions_login(){
			$index = isset($_SESSION[static::$token.'_uid']) ? $_SESSION[static::$token.'_uid'] : 0;
			$configs = self::get_config();
			
			$rid = isset($configs['user_role'][$index]) ? $configs['user_role'][$index] : false;

			$pids = isset($configs['roles'][$rid]) ? $configs['roles'][$rid] : [];
			
			$return = [];
			
			foreach($pids as $key){
				$return[] = $configs['permission'][$key];
			}
			
			self::$deal = $return;
			return self::init();
		}
		
		//登入
		public static function login($input, $check_version = true){
			if($check_version) self::check_version();
			$res = self::api('?method=login', $input);
			if($res['status']){
				$_SESSION[static::$token.'_uid'] = (int) $res['data']['api_id']-100000;
			}
			return $res;
		}
		
		//api id是否存在
		public static function isset_api_id($api_id){
			$response = self::api('?method=isset_api_id', ['api_id' => $api_id]);
			if($response['status']){
				return ($response['data'] !== false);
			}
			return false;
		}
		
		public static function create_user($data = []){
			
			return self::api('?method=create_user', $data);
		}
		
		//刪除使用者
		public static function delete_user($api_id){
			
			return self::api('?method=delete_user', ['api_id' => $api_id]);
		} 
		
		public static function logout(){
			unset($_SESSION[static::$token.'_uid']);  
		}
	}
	
	
	
	function can($object = null, $action = null){
		
		$numargs = func_num_args();

		$search_key = ($numargs == 1) ? $object : $object.'.'.$action;
		
		$uid = Cfdcenter::$token.'_uid';
		
		if(!isset($_SESSION[$uid])) return false;
		
		$permission = Cfdcenter::permissions_login()->_all();
			
		if(isset($permission[$search_key])) return true;
		
		return false;
	} 
